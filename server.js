
var http = require('http');
var path = require('path');

var async = require('async');
var express = require('express');
var http=require('http');
var https = require("https");
var jsdom = require("jsdom").jsdom;
var doc = jsdom();
var window = doc.defaultView;
var $ = require('jquery')(window);
var request = require('request');
var fs = require("fs");
var bodyParser = require('body-parser');
var router = express();
var youtubedl = require('youtube-dl');
const cheerio = require('cheerio');
var createClient = require("webdav");
var client = createClient(
    "http://45.76.14.41/owncloud/remote.php/webdav/",
    "owncloud",
    "phung95"
);
var mongoose   = require('mongoose');
mongoose.connect('mongodb://phungvm:phungvm@ds133981.mlab.com:33981/music'); // connect to our database
var Music     = require('./app/models/music');
router.use(bodyParser.json()); // support json encoded bodies
router.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
$.support.cors=true;

var server = http.createServer(router);
var io = require('socket.io').listen(server);
router.use(express.static(path.resolve(__dirname, 'client')));
var json ='';
router.get('/get/:song', function (req, response){
  var result = req.params.song;
  var temp = result.split(":");
  var song = '';
  song = temp[1].replace(/ /gi, "+");
  var options = {
    host: 'm.nhaccuatui.com',
    path: '/tim-kiem/bai-hat?q='+song
  };
  var htmls = '';
  http.get(options,function(res){
    res.on('data', function(body){
      htmls += body;
    });
    res.on('end', function(){
      console.log("success");
      jsdom.env({  
        html: htmls, 
        scripts: [
          'http://code.jquery.com/jquery-1.5.min.js'
        ], 
        done: function (err, window) {
        var $ = window.jQuery; 
        var obj ={};
        var arr= [];
        $.each($($(".bgmusic")), function(key,value){
          if(key <=10){
            var url = $($($($($(".bgmusic")[key]).children())[0]).children()).attr("href");
            var nameSong = $($($($($(".bgmusic")[key]).children())[0]).children()).attr("title");
            var singer = $($($($($(".bgmusic")[key]).children()[1])[0]).children()[0]).attr("alt")
            var temp = url.split(".");
            var id = temp[temp.length - 2];
            //, 'song' : downloadUrl
            obj = {'id': id,'nameSong':nameSong , 'name' : singer};
            arr.push(obj);
          } else return false;
        })
        var temp = JSON.stringify(arr);
        json = temp.replace(/\\/g, "");
        response.json(json);
        if (err) {
          console.log('Inside error, fetching product line items failed');
        }
      }
      });
    });
  })
});

router.get('/music:uid', function(req,res){
  var url = req.url;
  var temp = url.split(":");
	var fileId = temp[1]; 
	var file = __dirname + '/music/' + fileId;
	fs.exists(file,function(exists){
		if(exists)
		{
			var rstream = fs.createReadStream(file);
			rstream.pipe(res);
		}
		else
		{
			res.send("Its a 404");
			res.end();
		}
  });
});


var bodySaved;
// router.post('/post', function(req, res) {
//   var bool = req.body.bool;
// });

io.on('connection', function(socket){
  console.log("user connected");
  socket.on('sendid', function(sendId){
    //var realId = sendId + "_128";
    var realId = sendId;
    var url = "http://www.nhaccuatui.com/download/song/"+ realId;
    request.get(url, function(err, response, body){
      var temp = JSON.parse(body);
      if(temp.data != null){
        var downloadUrl = temp.data.stream_url.replace(/\\/g, "");
        var requestURL = {
          method: 'GET',
          url: downloadUrl,
          encoding: null
        };
        var fileID = realId + ".mp3";
        var query  = Music.where({ musicId: fileID});
		    query.findOne(function (err, mus) {
		      if(mus == null){
		        request(requestURL, function(error, resp, chunkz) {
            if(Buffer.isBuffer(chunkz)){
            // fs.writeFile('music/'+fileID, chunkz, function () {
            //   var link = "/music:"+fileID;
            //   socket.emit("link",link);
            // });
		            console.log("eo vao day");
		            client
                  .putFileContents("/data/"+fileID, chunkz, { format: "binary" })
                  .then(function(text){ socket.emit("link","http://45.76.14.41/owncloud/index.php/s/20KTWGC8PvsmksS/download?path=%2F&files="+fileID)})
                  .catch(function(err) {
                    console.error(err);
                  });
		            var song = new Music();
		            song.musicId = fileID;
		            song.url= "http://45.76.14.41/owncloud/index.php/s/20KTWGC8PvsmksS/download?path=%2F&files="+fileID;
		            song.save();
            }
            });
		      }else socket.emit("link",mus.url);
		    });
      }else socket.emit("error","This song has licensed");
    });
  });
  socket.on('sendnextid', function(id){
    var url = "http://45.76.14.41/owncloud/index.php/s/20KTWGC8PvsmksS/download?path=%2F&files="+id + ".mp3";
    socket.emit('sendnextid',url);
  });
  socket.on('sendyoutube', function(link){
    var video = youtubedl(link,
      // Optional arguments passed to youtube-dl. 
      ['--format=18'],
      // Additional options can be given for calling `child_process.execFile()`. 
      { cwd: __dirname });
    // Will be called when the download starts. 
    video.on('info', function(info) {
      console.log('Download started');
      console.log('filename: ' + info.filename);
      console.log('size: ' + info.size);
    });
    video.pipe(fs.createWriteStream('music/myvideo.mp4'));
    var link = "/music:myvideo.mp4";
    socket.emit('sendvideolink',link);
  });
});

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});
