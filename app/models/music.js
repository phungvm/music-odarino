var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var MusicSchema   = new Schema({
	musicId: String,
	url: String,
});

module.exports = mongoose.model('Music', MusicSchema);